<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Search Job</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{url('frontend/scss/main.css')}}">
    <script src="{{url('frontend/js/script.js')}}"></script>
</head>

<body>

    <div class="container">
        <nav class="navbar navbar-light justify-content-between my-4">
            <a class="navbar-brand" href="https://jobseeker.software/jobs">
                <img src="{{url('frontend/images/logo_brand.png')}}" width="20%" alt="">
            </a>
            <form action="https://jobseeker.software/jobs" class="form-inline">
                <button class="btn btn-login my-2 my-sm-0" type="button">Login</button>
            </form>
        </nav>
    </div>

    <section class="header-title">
        <div class="container ">
            <div class="row d-flex align-items-center">
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="row">
                        <div class="col">
                            <h2 class="fs-1">
                                We help you
                            </h2>
                            <h2 class="display-4" style="font-weight: bold"> <strong>build your job portal</strong></h2>
                        </div>
                    </div>
                    <div class="row">
                        <p>
                            Why use third party when you can build your own, <br>customizable job portal for your
                            specific requirements
                        </p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <img src="{{url('frontend/images/bg-1.png')}}" width="100%" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class="header">
        <div class="container py-4 mt-5">
            <div class="row py-3">
                <div class="col-12 text-center ">
                    <h2>
                        Powered by
                        <br>
                        Applicant Tracking System
                    </h2>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-12 text-center">
                    <p class="fw-lighter">Applicant Tracking System or ATS is software that streamlines and automates
                        the end-to-end recruitment process for an employer or recruiter
                        from source to hire. 99% of Fortune 500 companies use ATS to make their hiring process faster,
                        cheaper and more effective</p>
                </div>
            </div>

            <div class="row">
                <div class="col-12 text-center py-5">
                    <button class="btn btn-login">Get Started</button>
                </div>
            </div>

        </div>
    </section>
    <section class="portal">
        <div class="bg-gradient-color">
            <div class="container-fluid py-3">
                <div class="row py-5">
                    <div class="col-12 text-center">
                        <h2 class="text-white">Ready to build your own job portal?</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center py-3">
                        <button class="btn btn-login">Get Started</button>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="feature-product">
        <div class="container ">
            <div class="row d-flex align-items-center mb-5">

                <div class="col-sm-12 col-md-12 col-lg-6">
                    <img src="{{url('frontend/images/feature_foto1.png')}}" width="100%" alt="">
                </div>
                <div class="col-sm-12 col-md-12  col-lg-6">
                    <div class="row py-2">
                        <div class="col">
                            <h2 class="fs-1 fw-regular">
                                High Quality Candidates
                            </h2>
                        </div>
                    </div>
                    <div class="row py-2">
                        <p class="fw-light">
                            Get more high quality candidates faster and at lower cost per hire
                        </p>
                    </div>

                    <div class="row">
                        <a href="" class="btn-no-border">Get Started -></a>
                    </div>

                </div>
            </div>


            <div class="row d-flex align-items-center">
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="row">
                        <div class="col">
                            <h2 class="fs-1 fw-regular">
                                Saved Talent Pool
                            </h2>
                        </div>
                    </div>
                    <div class="row">
                        <p class="fw-lighter">
                            Boost employer brand and build your own talent pool
                        </p>
                    </div>

                    <div class="row">
                        <a href="" class="btn-no-border">Get Started -></a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <img src="{{url('frontend/images/feature_foto2.png')}}" width="100%" alt="">
                </div>
            </div>
            <div class="row d-flex align-items-center">

                <div class="col-sm-12 col-md-12 col-lg-6">
                    <img src="{{url('frontend/images/feature_foto3.png')}}" width="100%" alt="">
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="row">
                        <div class="col">
                            <h2 class="fs-1 fw-regular">
                                Automated hiring Process & Insights
                            </h2>
                        </div>
                    </div>
                    <div class="row">
                        <p class="fw-lighter">
                            Leverage insights from automated hiring process to win the war for talent
                        </p>
                    </div>

                    <div class="row">
                        <a href="" class="btn-no-border">Get Started -></a>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="title-search-job">
        <div class="container">
            <div class="row py-5">
                <div class="col-12 text-center">
                    <h2>Search any jobs for You!</h2>
                </div>
            </div>
            <div class="row py-3">
                <center>
                    <div class="col-12">
                        <form action="" class="search-bar ">
                            <input type="text" id="jobSearch" placeholder="Position, City or Company Name"
                                name="searh-job">
                            <button type="submit" style="border: none; padding:0;background:none"><img
                                    src="{{url('frontend/images/btn-search.png')}}" width="100%" alt=""></button>
                        </form>
                    </div>
                </center>
            </div>
        </div>
    </section>

    <section class="search-job my-5">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-lg-4">
                    <h2 class="my-4"><strong>AVAILABLE JOBS</strong></h2>
                    <div class="row">
                        <p><strong>Top 5 Locations</strong></p>
                        <ul class="top-location list-unstyled ms-2">
                            <li class="fw-semibold">Jakarta</li>
                            <li class="fw-semibold">Surabaya</li>
                            <li class="fw-semibold">Bali</li>
                            <li class="fw-semibold">Tangerang</li>
                            <li class="fw-semibold">Bandung</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-lg-8 " id="search-content">

                </div>
                <div class="float-end">

                    <div class="col-md-12 col-sm-12 col-lg-8 float-md-end">
                        <a class="btn col-12 border-0" href="https://jobseeker.software/jobs"
                            style="background-color: #E4007E; color:white; height:49px;" type="button"> See all
                            vacancies</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer class="bg-dark">
        <div class="container section-footer">
            <div class="row">
                <div class="col-12 mt-3">
                    <img src="{{url('frontend/images/jobseeker_white.png')}}" width="72px" height="67px" alt="">
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-6">
                    <div class="row">
                        <div class="col-auto">
                            <p><Strong class="text-white">INDONESIA <br>Jakarta</Strong></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <p class="text-white">
                                AD Premier Office Park, 9th Floor
                                Jl. TB Simatupang No.5, Ragunan, Pasar Minggu
                                South Jakarta City, Jakarta 12550

                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-auto">
                            <p><Strong class="text-white">Bali</Strong></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <p class="text-white">
                                Jl. Karang Mas, Jimbaran, South Kuta
                                Kabupaten Badung, Bali, 80361
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-auto">
                            <p><Strong class="text-white">SINGAPORE</Strong></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <p class="text-white">

                                10 Anson Road #22-02 International Plaza,
                                Singapore, 079903

                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="col-md-2">
                        <p><Strong class="text-white">FOR EMPLOYER</Strong></p>
                    </div>
                    <ul style="color: white" class="list-unstyled">
                        <li><a class="text-white btn p-0 m-0" href="">jobseeker.partners</a></li>
                        <li><a class="text-white btn p-0 m-0" href="">jobseeker.services</a></li>
                        <li><a class="text-white fw-bold" href="">jobseeker.software</a></li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <div class="col-auto">
                        <p><Strong class="text-white">FOR CANDIDATE</Strong></p>
                        <a class="text-white btn p-0 m-0" href="">jobseeker.app</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-auto">
                    <img src="{{url('frontend/icons/Email.png')}}" width="24px" height="24px" alt="">

                </div>
                <div class="col-auto ">
                    <p class="text-white">info@jobseeker.company</p>
                </div>
            </div>
            <div class="row">
                <div class="col-auto">
                    <img src="{{url('frontend/icons/Whatsapp.png')}}" width="24px" height="24px" alt="">

                </div>
                <div class="col-auto ">
                    <p class="text-white">+62 813 1881 7887</p>
                </div>
            </div>
            <div class="row">
                <div class="col-auto">
                    <img src="{{url('frontend/icons/Facebook.png')}}" width="24px" height="24px" alt="">
                </div>
                <div class="col-auto">
                    <img src="{{url('frontend/icons/Instagram.png')}}" width="24px" height="24px" alt="">
                </div>
                <div class="col-auto">
                    <img src="{{url('frontend/icons/LinkedIn.png')}}" width="24px" height="24px" alt="">
                </div>
                <div class="col-auto">
                    <img src="{{url('frontend/icons/tiktok.png')}}" width="24px" height="24px" alt="">
                </div>
                <div class="col-auto">
                    <img src="{{url('frontend/icons/youtube.png')}}" width="24px" height="24px" alt="">
                </div>
                <div class="col-auto">
                    <img src="{{url('frontend/icons/telegram.png')}}" width="24px" height="24px" alt="">
                </div>

            </div>
            <div class="row py-3">
                <hr class="text-white">
            </div>
            <div class="row">
                <div class="d-flex">
                    <div class="p-1 flex-grow-1 text-white">Copyright © 2022 JobSeekerApps Pte. Ltd</div>
                    <div class="p-2 text-white">Privacy Policy</div>
                    <div class="p-2 text-white">Term of Service</div>
                </div>

            </div>
        </div>

    </footer>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js">
    </script>
    <script>
        $(document).ready(function () {
           var html= $('#search-content');
           
            $.ajax({
                url:"api/vacancy",
                type:"get",
                dataType:'json',
                success:function(data){
                    
                    $.each(data.data, function(index, value) {    
                        html.append(searchContent(value))
                    });
                }
            });
            
        });

        function searchContent(value) {
            return `<div class="card job-data border-0">
                        <div class="row ms-1">
                            <div class="col-md-9 col-sm-12">
                                <h5 class="fw-bold">${value.job_position}</h5>
                            </div>
                            <div class="col-3">
                                <h3 style="color: #20A6FC">${value.city_name}</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-auto text-lighter">
                                            Requirements:
                                        </div>
                                        <div class="col-md-6 col-lg-4">
                                            ${value.requirements}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-md-2 fw-lighter col-lg-auto">
                                            Salary:
                                        </div>
                                        <div class="col-md-3 fw-lighter">
                                            IDR ${value.min_salary} - ${value.max_salary} Milion
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row py-4">
                                <div class="col-md-9 col-sm-12">
                                    <p class="fst-italic fw-lighter">Posted 10 mins ago</p>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <button class="btn btn-login">Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </div>

                   `
        }
        $('.search-bar').submit(function(e){
        e.preventDefault();
        var html= $('#search-content').html('');
        
        $.ajax({
            url:"api/vacancy-search/"+$('#jobSearch').val(),
                type:"get",
                dataType:'json',
                success:function(data){
                    $.each(data.data, function(index, value) {    
                        html.append(searchContent(value));
                    });
                },
                error:function (data) {
                    alert('Data Not Found');
                }   
                
        });
       });
       $("li").click(function ()
        {       
        var param = $(this).text();
        var html= $('#search-content').html('');
        $.ajax({
            url:"api/vacancy-city/"+param,
                type:"get",
                dataType:'json',
                success:function(data){
                    if (data.data.length == 0) {
                        alert('Data Not Found');
                    }
                    $.each(data.data, function(index, value) {    
                        html.append(searchContent(value));
                    });
                }
                
        });
        
    });
    </script>

</body>

</html>