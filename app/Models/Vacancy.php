<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    use HasFactory;
    protected $fillable = ["job_postion",
    "city_name",
    "company_name",
    "min_salary",
    "max_salary",
    "requirements",
];
}
