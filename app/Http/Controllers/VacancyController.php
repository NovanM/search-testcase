<?php

namespace App\Http\Controllers;

use App\Models\Vacancy;
use Illuminate\Http\Request;

class VacancyController extends Controller
{
    //
    public function index()
    {
        $data = Vacancy::paginate(4);
        return response($data, 200);
    }

    public function search($param)
    {

        if ($param) {
            $data = Vacancy::query()
                ->where('job_position', 'LIKE', "%{$param}%")
                ->orWhere('city_name', 'LIKE', "%{$param}%")
                ->orWhere('company_name', 'LIKE', "%{$param}%")
                ->paginate(4);

            return response($data, 200);
        }
    }
    public function get_by_city_name($param)
    {
        $data = Vacancy::where('city_name', '=', $param)->paginate(4);
        return response($data, 200);
    }
}
