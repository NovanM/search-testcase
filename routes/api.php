<?php

use App\Http\Controllers\VacancyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('vacancy-search/{param}',[VacancyController::class,'search'])->name('search');
Route::get('vacancy',[VacancyController::class,'index'])->name('index');
Route::get('vacancy-city/{param}',[VacancyController::class,'get_by_city_name'])->name('vacancy-city-name');